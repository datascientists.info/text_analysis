# database stuff
from sqlalchemy import create_engine
from sqlalchemy import create_engine, MetaData, TEXT, Integer, Table, Column, ForeignKey
import psycopg2

import pandas as pd

import config

"""
Class to handle database related stuff using SQLAlchemy as interface
"""
class DB:
    engine = None

    """
    constructor that opens a database connections with credentials from config.py
    """
    def __init__(self):
        # open connection to database
        try:
            self.engine = create_engine("postgresql://" + config.DATABASE_CONFIG['user'] + ":" + config.DATABASE_CONFIG['password'] +
                                   "@" + config.DATABASE_CONFIG['host'] + ":" + config.DATABASE_CONFIG['port'] +
                                   "/" + config.DATABASE_CONFIG['db_name'])
        except Exception as e:
            print ("Error establishing connection " + str(e))

    """
    functions that selects data from database and returns a pandas dataframe with the result
    parameters:
    - s_sql:     String of query that should be passed into database
    - index_col: give name of index column, if selected, so pandas does not create a new one
    """
    def select(self, s_sql, index_col=None):
        try:
            return pd.read_sql_query(s_sql, self.engine, index_col=index_col)
        except Exception as e:
            print ("Error in your sql. Please check: " + s_sql + " " + str(e))

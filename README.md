# Notebooks for Textmining

## Usage
### Prerequistes

   1. install python3
   2. install pip3
   3. install requirements by running "pip3 install -r requirements.txt"
   4. adapt config.py values to your own

### Description
#### topic_modelling notebook
This notebook selects data from a database and then compiles a LDA model from it.

#### lstm_keras notebook
WIP

#### Helper classes
There are two helper classes:
- db.db.DB:                     takes care of all database handling using SQLAlchemy
- misc.helper_functions.Helper: takes care of text preprocessing
   
## Licensing
MIT License
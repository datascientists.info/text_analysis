from bs4 import BeautifulSoup
import re

# stem words
from nltk.stem import SnowballStemmer

# stop words for word2vec
from stop_words import get_stop_words

"""
Class Helper with functions to help preprocessing texts
"""
class Helper:
    """
    text_to_words takes a text as string and returns an array of words.
    words are stemmed using snowball stemmer
    variables:
    - text_raw:             unprocessed text as string
    - stop_word_language:   language stop words should be compared with

    returns: array of words
    """
    def text_to_words(text_raw, language_iso_2="en"):
        if language_iso_2 == "en":
            language = "english"
        elif language_iso_2 == "de":
            language = "german"
        else:
            language = "english"

        stemmer = SnowballStemmer(language)

        text = text_raw

        stop_words = get_stop_words(language_iso_2)
        text = str(text)

        letters_only = ""
        # Remove non-letters
        if len(text) > 0:
            letters_only = re.sub("[^\W0-9]", " ", text, re.UNICODE | re.IGNORECASE)

        words = letters_only.lower().split()

        stops = set(stop_words)

        meaningful_words = [stemmer.stem(w) for w in words if not w in stops]

        return( " ".join( meaningful_words ))